<!doctype html>

<html lang="es-Es">

<head>

<meta charset="utf-8"/>

<meta name="viewport" content="width=device-width , initial-scale=1 ,maximum-scale=1" />

	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/reset.css" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/modal.css" />
	<style type="text/css">

	.white-popup-block {
		background: #000;
		padding: 20px 30px;
		text-align: left;
		max-width: 1080px;
		margin: 10px auto;
		position: relative;
}

	</style>
<!--[if IE]>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/ie.css" />
<![endif]-->


<!--=========================js-->

<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); wp_head(); ?>

<script src="<?php bloginfo('template_url'); ?>/js/modernizr.custom.84316.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/modal.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/ga.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/ender.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/selectnav.js"></script>
<script>
    $(function(){
    	
      // bind change event to select
      $('#jumpMenu').bind('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });


    });
</script>

	<script>
		domready(function(){		
			selectnav('menu_mobil', {
				label: 'MENU',
				nested: true,
				indent: '-'
			});
			//hijs();
			prettyPrint()
		})
	</script>

</head>

<body>

<div id="content">

	

<header>

	

	<h1 itemprop="name"><a href="/" title="Corazon Contento"><img src="<?php bloginfo('template_url'); ?>/img/logo.png"></a></h1>



	<div class="toping">



	<div class="sticker">
    <?php $lang = pll_current_language(); ?>
	<?php if($lang == "en") { ?>
		<a href="http://corazoncontentogranada.org/donations">
			<img src="<?php bloginfo('template_url'); ?>/img/sticker_en.png"></a>
	<?php  }else {
	?>
	<a href="http://corazoncontentogranada.org/donaciones/">
			<img src="<?php bloginfo('template_url'); ?>/img/sticker.png"></a>
	<?php

	} ?>

	</div><!--sticker-->

	<div class="socials">

		<a href="https://twitter.com/Corazon_Content" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/twitter.png"></a>

		<a href="http://www.pinterest.com/albertomc3453/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/pinterest.png"></a>

		<a href="https://www.facebook.com/CentroIntegralDeDesarrolloCorazonContento" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/facebook.png"></a>

		<a href="http://www.youtube.com/user/cidcorazoncontento" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/youtube.png"></a>

	</div><!-- #socials -->



	<div class="idiomas">
	<?php 

	function dameURL(){

	$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
	return $url;
	}

    $en_link = '?lang=en';
    $es_link = '?lang=es';
    
    if(is_single() || is_page() || is_category()){
        $translated_en = (int)pll_get_post($post->ID, 'en_US');
        $translated_es = (int)pll_get_post($post->ID, 'es_ES');
        
        $en_link = get_permalink($translated_en);
        $es_link = get_permalink($translated_es);
    }
    if(is_category()){
        $term = get_queried_object();
        $translated_en = (int)pll_get_term($term->term_id, 'en_US');
        $translated_es = (int)pll_get_term($term->term_id, 'es_ES');
        
        $en_link = get_category_link($translated_en);
        $es_link = get_category_link($translated_es);
    }
    $lang = pll_current_language();
	if($lang == "en")
	{
        
	 ?>

		<a href="<?php echo $en_link?>" hreflang="en_US">English</a>

		<a href="<?php echo $es_link?>" hreflang="es_ES">Spanish</a>


		<?php
		}else
		{
			?>
			<a href="<?php echo $en_link?>" hreflang="en_US" >English</a>

		     <a href="<?php echo $es_link?>" hreflang="es_ES">Español</a>
			<?php
		}
	?>

	</div><!-- #socials -->





	<div class="navigation">



	<nav>


	<?php  if( $lang =="en") 

	{
		?>
		<!-- Multi-level Navigation Plugin v2.3.6 by Ryan Hellyer ... http://geek.ryanhellyer.net/multi-level-navigation/ -->
<div id="pixopoint_menu_wrapper1">
<div id="pixopoint_menu1">		
<ul class="sf-menu" id="suckerfishnav">
<li><a <?php if(isset($_GET) and $_GET['stick']=="home") {?> class="current" <?php }  ?> 
       <?php if(isset($_GET) and $_GET['active']=="tr") {?> class="current" <?php }  ?>href="http://corazoncontentogranada.org/?stick=home&active=tr">The center</a>
<ul>
<li><a <?php if(isset($_GET) and $_GET['stick']=="about") {?> class="current bgl" <?php }  ?> href="http://corazoncontentogranada.org/about-us/?stick=about&active=tr">Abouts us</a><div class="line"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="object") {?> class="current bgl2" <?php }  ?> href="http://corazoncontentogranada.org/our-objectives/?stick=object&active=tr">Our objectives</a><div class="line"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="welcome") {?> class="current bgl2" <?php }  ?> href="http://corazoncontentogranada.org/welcome/?stick=welcome&active=tr">welcome</a><div class="line"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="history") {?> class="current bgl2" <?php }  ?> href="http://corazoncontentogranada.org/history?stick=history&active=tr">history</a><div class="line"></div></li>
</ul>
<li>
<a <?php if(isset($_GET) and $_GET['stick']=="services") {?> class="current" <?php }  ?> 
   <?php if(isset($_GET) and $_GET['active']=="services") {?> class="current" <?php }  ?>href="http://corazoncontentogranada.org/occupational-therapy-workshop/?stick=services&active=services">Services</a>
<ul>
<li><a <?php if(isset($_GET) and $_GET['stick']=="terapia") {?> class="current bgl3" <?php }  ?> href="http://corazoncontentogranada.org/occupational-therapy-workshop/?stick=terapia&active=services">Occupational therapy workshop</a><div class="line2"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="estimulacion") {?> class="current bgl4" <?php }  ?>href="http://corazoncontentogranada.org/early-learning-center/?stick=estimulacion&active=services">center of early stimulation</a><div class="line2"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="escuela") {?> class="current bgl5" <?php }  ?>href="http://corazoncontentogranada.org/school-of-family/?stick=escuela&active=services">school family</a></li>
</ul><li><a <?php if(isset($_GET) and $_GET['stick']=="boletines") {?> class="current" <?php }  ?> href="javascript:void(0);">Notices</a>
<ul>
<li><a <?php if(isset($_GET) and $_GET['stick']=="newsletters") {?> class="current bgl3" <?php }  ?> href="http://corazoncontentogranada.org/newsletters/?stick=newsletters">Newsletters</a><div class="line3"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="present") {?> class="current bgl3" <?php }  ?> href="http://corazoncontentogranada.org/info/notice/?stick=present">Present</a></li>
</ul>

<li><a <?php if(isset($_GET) and $_GET['stick']=="colabora") {?> class="current" <?php }  ?>
            <?php if(isset($_GET) and $_GET['active']=="collaborate") {?> class="current" <?php }  ?> href="http://corazoncontentogranada.org/volunteering/?stick=colabora&active=collaborate">Collaborate with us</a>
<ul>
<li><a <?php if(isset($_GET) and $_GET['stick']=="voluntariado") {?> class="current bgl10" <?php }  ?> href="http://corazoncontentogranada.org/volunteering/?stick=voluntariado&active=collaborate">Volunteer</a><div class="line5"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="donaciones") {?> class="current bgl11" <?php }  ?>href="http://corazoncontentogranada.org/donations?stick=donaciones&active=collaborate">donations</a><div class="line5"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="rse") {?> class="current bgl12" <?php }  ?> href="http://corazoncontentogranada.org/corporate-social-responsibility-csr/?stick=rse&active=collaborate">Corporate Social Responsibility (CSR)</a><div class="line5"></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="catalogo") {?> class="current bgl12" <?php }  ?>
       <?php if(isset($_GET) and $_GET['active']=="cotalogo") {?> class="current" <?php }  ?> href="http://corazoncontentogranada.org/catalog?stick=catalogo&active=cotalogo">catalog</a></li>
</ul></ul>
	</div>
</div>
		<?php
	}else{
		?>
<!-- Multi-level Navigation Plugin v2.3.6 by Ryan Hellyer ... http://geek.ryanhellyer.net/multi-level-navigation/ -->
<div id="pixopoint_menu_wrapper1">
<div id="pixopoint_menu1">		
<ul class="sf-menu" id="suckerfishnav">
<li class="primero">
<a <?php if(isset($_GET) and $_GET['stick']=="home") {?> class="current" <?php }  ?>
   <?php if(isset($_GET) and $_GET['active']=="tr") {?> class="current" <?php }  ?> href="http://corazoncontentogranada.org/?stick=home&active=tr">el centro</a>
<ul>
<li><a <?php if(isset($_GET) and $_GET['stick']=="about") {?> class="current bgl" <?php }  ?> href="http://corazoncontentogranada.org/quienes-somos/?stick=about&active=tr">quienes somos</a><div class="line"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="object") {?> class="current bgl2" <?php }  ?> href="http://corazoncontentogranada.org/nuestros-objetivos/?stick=object&active=tr">nuestros objetivos</a><div class="line"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="bienvenidos") {?> class="current bgl2" <?php }  ?> href="http://corazoncontentogranada.org/bienvenidos/?stick=bienvenidos&active=tr">bienvenidos</a><div class="line"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="histora") {?> class="current bgl2" <?php }  ?> href="http://corazoncontentogranada.org/historia/?stick=historia&active=tr">historia</a><div class="line"></div></li>
</ul>
<li>
<a <?php if(isset($_GET) and $_GET['stick']=="services") {?> class="current" <?php }  ?>
   <?php if(isset($_GET) and $_GET['active']=="ser") {?> class="current" <?php }  ?>href="http://corazoncontentogranada.org/taller-de-terapia-ocupacional/?stick=services">Servicios</a>
<ul>
<li><a <?php if(isset($_GET) and $_GET['stick']=="terapia") {?> class="current bgl3" <?php }  ?> href="http://corazoncontentogranada.org/taller-de-terapia-ocupacional/?stick=terapia&active=ser">taller de terapia ocupacional</a><div class="line2"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="estimulaci&oacute;n") {?> class="current bgl4" <?php }  ?> href="http://corazoncontentogranada.org/centro-de-estimulacion-temprana/?stick=estimulacion&active=ser">centro de estimulaci&oacute;n temprana</a><div class="line2"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="escuela") {?> class="current bgl5" <?php }  ?> href="http://corazoncontentogranada.org/escuela-de-familia/?stick=escuela&active=ser">escuela de familia</a></li>
</ul><li><a <?php if(isset($_GET) and $_GET['stick']=="boletines") {?> class="current" <?php }  ?> href="javascript:void(0);">Noticias</a>
<ul>
<li><a <?php if(isset($_GET) and $_GET['stick']=="boletines") {?> class="current bgl5" <?php }  ?> href="http://corazoncontentogranada.org/boletines/?stick=boletines">Boletines</a><div class="line6"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="actualidad") {?> class="current bgl5" <?php }  ?> href="http://corazoncontentogranada.org/info/noticias/?stick=actualidad">Actualidad</a></li>

</ul>
<li>
<a <?php if(isset($_GET) and $_GET['stick']=="colabora") {?> class="current" <?php }  ?> 
   <?php if(isset($_GET) and $_GET['active']=="cola") {?> class="current" <?php }  ?>  href="http://corazoncontentogranada.org/voluntariado/?stick=colabora&active=cola">Colabora con nosotros</a>
<ul>
<li><a <?php if(isset($_GET) and $_GET['stick']=="voluntariado") {?> class="current bgl10" <?php }  ?> href="http://corazoncontentogranada.org/voluntariado/?stick=voluntariado&active=cola">voluntariado</a><div class="line4"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="donaciones") {?> class="current bgl11" <?php }  ?> href="http://corazoncontentogranada.org/donaciones/?stick=donaciones&active=cola">donaciones</a><div class="line4"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="rse") {?> class="current bgl12" <?php }  ?> href="http://corazoncontentogranada.org/responsabilidad-social-empresarial-rse/?stick=rse&active=cola">responsabilidad social empresarial (RSE)</a><div class="line4"></div></li>
<li><a <?php if(isset($_GET) and $_GET['stick']=="catalogo") {?> class="current bgl12" <?php }  ?>
       <?php if(isset($_GET) and $_GET['active']=="cotalogo") {?> class="current" <?php }  ?> href="http://corazoncontentogranada.org/catalogo/?stick=catalogo&active=cotalogo">cat&aacute;logo</a></li>
</ul></ul>
	</div>
</div>
		<?php
	} 

	?>

	
				<div class="mobile">
					<?php if($lang=="en") { ?>
						<section>
							<ul id="menu_mobil">
								<li><a href="http://corazoncontentogranada.org">The Center</a></li>
								<li><a href="http://corazoncontentogranada.org/about-us">- About us</a></li>
								<li><a href="http://corazoncontentogranada.org/our-objectives">- Our objectives</a></li>
								<li><a href="http://corazoncontentogranada.org/welcome">- Welcome</a></li>
								<li><a href="http://corazoncontentogranada.org/history">- History</a></li>
								<li><a href="http://corazoncontentogranada.org/occupational-therapy-workshop">Services</a></li>
								<li><a href="http://corazoncontentogranada.org/occupational-therapy-workshop">- Occupational therapy workshop</a></li>
								<li><a href="http://corazoncontentogranada.org/early-learning-center">- Center Of Early Stimulation</a></li>
								<li><a href="http://corazoncontentogranada.org/school-of-family">- School Family</a></li>
								<li><a href="http://corazoncontentogranada.org/newsletters">Notices</a></li>
								<li><a href="http://corazoncontentogranada.org/newsletters">- Newsletters</a></li>
								<li><a href="http://corazoncontentogranada.org/info/notice">- Present</a></li>
								<li><a href="http://corazoncontentogranada.org/volunteering">Collaborate with us</a></li>
								<li><a href="http://corazoncontentogranada.org/volunteering">- Volunteering</a></li>
								<li><a href="http://corazoncontentogranada.org/donations">- Donations</a></li>
								<li><a href="http://corazoncontentogranada.org/corporate-social-responsibility-csr">- Corporate Social Responsibility (RSE)</a></li>

								
							</ul>
						</section>
					<?php  }else {
					?>
					<section>
							<ul id="menu_mobil">
								<li><a href="http://corazoncontentogranada.org/">El Centro</a></li>
								<li><a href="http://corazoncontentogranada.org/quienes-somos/">- Quienes Somos</a></li>
								<li><a href="http://corazoncontentogranada.org/nuestros-objetivos/">- Nuestros Objetivos</a></li>
								<li><a href="http://corazoncontentogranada.org/bienvenidos/">- Bienvenidos</a></li>
								<li><a href="http://corazoncontentogranada.org/historia/">- Historia</a></li>
								<li><a href="http://corazoncontentogranada.org/taller-de-terapia-ocupacional/">Servicios</a></li>
								<li><a href="http://corazoncontentogranada.org/taller-de-terapia-ocupacional/">- Taller de terapia Ocupacional</a></li>
								<li><a href="http://corazoncontentogranada.org/centro-de-estimulacion-temprana/">- Centro de Estimulacion Temprana</a></li>
								<li><a href="http://corazoncontentogranada.org/escuela-de-familia/">- Escuela de Familia</a></li>
								<li><a href="http://corazoncontentogranada.org/boletines/">Noticias</a></li>
								<li><a href="http://corazoncontentogranada.org/boletines/">- Boletines</a></li>
								<li><a href="http://corazoncontentogranada.org/actualidad/">- Actualidad</a></li>
								<li><a href="http://corazoncontentogranada.org/voluntariado/">Colabora con Nosotros</a></li>
								<li><a href="http://corazoncontentogranada.org/voluntariado/">- Voluntariado</a></li>
								<li><a href="http://corazoncontentogranada.org/donaciones/">- Donaciones</a></li>
								<li><a href="http://corazoncontentogranada.org/responsabilidad-social-empresarial-rse/">- Responsabilidad Social Empresarial RSE</a></li>
								
								
							</ul>
						</section>
					<?php

					} ?>

		<div><!--mobile-->
			</nav>
		</div><!--navigation-->
	</div><!--toping-->

</header>