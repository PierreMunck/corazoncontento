<?php get_header(); ?>

<div class="contenido-home">
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>
			<h4>Publicado el <?php the_time('j/m/Y') ?></h4>
			<div class="thumbnail">
				<?php
					if ( has_post_thumbnail() ){
						the_post_thumbnail();
					}
				?>
			</div>
			<?php the_content(); ?>
		<?php endwhile; ?>
	<?php endif; ?>
</div><!--contenido-->

<?php get_footer(); ?>